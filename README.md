# xxiv

## 424. 

> los animales carnívoros no comen animales carnívoros a todo lo más que llegan
> es a nutrirse de animales omnívoros el cerdo el hombre se conoce que la yerba
> resta veneno a la carne la leche de la hembra de los animales herbívoros es
> agraz pero no tóxica la tímida gallina necrófaga no da leche y los polluelos
> viven con suma agilidad son muy flexibles y corredores la leche de la hembra
> de los animales carnívoros u omnívoros es dulzona y produce leucemia y otras
> debilidades  que  sumen  al  espíritu  en  la  confusión  no  es saludable un
> mar de leche por el que no se puede navegar

## 425. 

> un mar de leche en el que todos los navegantes son náufragos no hagas ningún
> postre de cocina con leche de hembra de animal carnívoro y aun omnívoro de
> loba de leona de mujer de cerda regenera tu sangre y tu sentimiento con leche
> de animal cobarde de oveja de jirafa de asna

----

    vim:ft=mkd
