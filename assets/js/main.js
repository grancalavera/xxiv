requirejs.config({ shim: { 'd3': {exports: 'd3'}} })
require(

  [
      "d3"
    , "vertical-layout"
    , "horizontal-layout"
    , "square-layout"
    , "sine-layout"
    , "text!./box-template.html"
  ]

, function(d3, verticalLayout, horizontalLayout, squareLayout, sineLayout, template) {

    var s = d3.select(".carousel")
      , min = d3.random.normal(10, 5)
      , max = d3.random.normal(70, 10)
      , step = d3.random.normal(4, 2)
      , clone = attach(template)
      , format = d3.format("1.1f")
      , layout = horizontalLayout()
      , sine = sineLayout()

    d3.select(".horizontal").on("click", selectLayout(horizontalLayout()))
    d3.select(".vertical").on("click", selectLayout(verticalLayout()))
    d3.select(".square").on("click", selectLayout(squareLayout()))
    d3.select(".randomize").on("click" , function() { randomizeData(); draw() })
    d3.select(".sine").on(
      "click"
    , function() {
        sine.phase(Math.random() * 2 * Math.PI)
        selectLayout(sine)()
      }
    )

    randomizeData()
    draw()

    function draw() {
      var update = s.selectAll("li")
        .data(layout(data), dataValue)

      update.enter()
          .select(clone)
          .style("top", "-100px")

      update.transition()
          .duration(450)
          .style("top", position("y"))
          .style("left", position("x"))
        .select("h3")
          .text(dataValue)

      update.exit()
        .transition()
          .duration(200)
          .style("top", "-100px")
          .remove()
    }

    function randomizeData() {
      var found = {}
      data = d3.range(min(), max(), step())
          .map(Math.round)
          .filter(
            function(d, i) {
              var key = String(d)
              if (found[key + ""]) return false
              found[key] = true
              return true
            }
          )
    }

    function position(key) {
      return function(d) {
        return d[key] + "px"
      }
    }

    function dataValue(d) {
      return d.data
    }

    function selectLayout(newLayout) {
      return function() {
        layout = newLayout
        draw()
      }
    }

    function attach(prototype) {
      var div = document.createElement("div")
        , element
      div.innerHTML = prototype
      element = div.firstElementChild
      return function() {
        return this.appendChild(element.cloneNode(true))
      }
    }
  }
)
