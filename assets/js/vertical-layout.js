define(
  [ "d3" ]
, function() {

    return function() {

      var verticalGap = 20
        , verticalOffset = 20
        , horizontalOffset = 40
        , cache = []

      function layout(data) {
        return data.map(pack)
      }

      layout.verticalGap = function(value) {
        if (!arguments.length) return verticalGap
        verticalGap = value
        return layout
      }

      return layout

      function pack(d, i) {
        return {
          data: d
        , x: left()
        , y: top(d, i)
        }
      }

      function top(d, i) {
        return verticalOffset + (i * verticalGap) 
      }

      function left() {
        return horizontalOffset
      }
    }
  }
)
