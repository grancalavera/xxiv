define(

  [ "d3" ]
, function() {

    return function() {

      var horizontalGap = 30
        , amplitude = 90
        , verticalOffset = 40
        , horizontalOffset = 50
        , phase = 0

      function layout(data) {
        dataLength = data.length
        return data.map(pack)
      }

      layout.phase = function(value) {
        if (!arguments.length) return phase
        phase = value
        return layout
      }

      return layout

      function pack(d, i) {
        return {
          data: d
        , x: left(d, i)
        , y: top(d, i)
        }
      }

      function left(d, i) {
        return horizontalOffset + (i *  horizontalGap) 
      }

      function top(d, i) {
        return verticalOffset 
            + (1 + Math.sin(phase + (2 * Math.PI * i / dataLength))) * amplitude 
      }
    }
  }
)
