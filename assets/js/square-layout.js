define(
  [ "d3" ]
, function() {

    return function() {

      var horizontalGap = 50
        , verticalGap = 50
        , verticalOffset = 40
        , horizontalOffset = 50
        , dataLength

      function layout(d) {
        dataLength = data.length
        return data.map(pack)
      }

      layout.horizontalGap = function(value) {
        if (!arguments.length) return horizontalGap
        horizontalGap = value
        return layout
      }

      layout.verticalGap = function(value) {
        if (!arguments.length) return verticalGap
        verticalGap = value
        return layout
      }

      return layout

      function pack(d, i) {
        return {
          data: d
        , x: left(d, i)
        , y: top(d, i)
        }
      }

      function left(d, i) {
        var side = Math.round(Math.sqrt(dataLength))
        return horizontalOffset + ((i % side) * horizontalGap)
      }

      function top(d, i) {
        var side = Math.round(Math.sqrt(dataLength))
        return verticalOffset + (Math.floor(i / side) * verticalGap)
      }
    }
  }
)
