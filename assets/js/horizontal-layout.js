define(

  ["d3"]
, function() {

    return function() {

      var horizontalGap = 50
        , verticalOffset = 40
        , horizontalOffset = 50

      function layout(data) {
        return data.map(pack)
      }

      layout.horizontalGap = function(value) {
        if (!arguments.length) return horizontalGap
        horizontalGap = value
        return layout
      }

      return layout

      function pack(d, i) {
        return {
            data: d
          , x: left(d, i)
          , y: top()
        }
      }

      function top () {
        return verticalOffset 
      }

      function left (d, i) {
        return horizontalOffset + (i * horizontalGap)
      }
    }
  }
)
